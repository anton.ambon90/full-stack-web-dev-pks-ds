<?php

namespace App\Http\Controllers;

use App\Roles;
use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
        /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table users
        $users = Users::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data User',
            'data'    => $users
        ], 200);

    }

     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find user by ID
        $user = Users::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data User',
            'data'    => $user
        ], 200);

    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'username'   => 'required',
            'email' => 'required',
            'name' => 'required',
            'role_id' => 'required',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $user = Users::create([
            'username'     => $request->username,
            'email'   => $request->email,
            'name'   => $request->email,
            'role_id'   => $request->role_id

        ]);

        //success save to database
        if($user) {

            return response()->json([
                'success' => true,
                'message' => 'User Created',
                'data'    => $user
            ], 201);

        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'User Failed to Save',
        ], 409);

    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $user
     * @return void
     */
    public function update(Request $request, Users $user)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'username'   => 'required',
            'email' => 'required',
            'name' => 'required',
            'role_id' => 'required',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find user by ID
        $user = Users::findOrFail($user->id);

        if($user) {

            //update post
            $user->update([
                'username'  => $request->username,
                'email'   => $request->email,
                'name'   => $request->email,
                'role_id'   => $request->role_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'User Updated',
                'data'    => $user
            ], 200);

        }

        //data user not found
        return response()->json([
            'success' => false,
            'message' => 'User Not Found',
        ], 404);

    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find user by ID
        $user = Users::findOrfail($id);

        if($user) {

            //delete user
            $user->delete();

            return response()->json([
                'success' => true,
                'message' => 'User Deleted',
            ], 200);

        }

        //data user not found
        return response()->json([
            'success' => false,
            'message' => 'User Not Found',
        ], 404);
    }
}