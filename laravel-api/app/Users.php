<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Users extends Model
{
    protected $fillable = ['username', 'email', 'name', 'role_id'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {

        parent::boot();
        static::creating(function ($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });

    }
    public function roles()
    {
      return $this->belongsTo('App\Roles');
    }

}
